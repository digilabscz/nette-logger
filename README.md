# DigiLabs Nette Logger

Elegant tool for sending debug log to multiple channels. The "handlers" configuration is used to send to individual channels (using handlers).\
\
Debug log is sent to each channel simultaneously and also physically stored in the file storage. This tool allows a live preview of the stored debug log using an special HTTP request.

## Configuration

Register logger as Nette extension:

```neon
extensions:
    # logger
    logger: Digilabscz\NetteLogger\DI\LoggerExtension
```

Configure logger & handlers:
```neon
logger:
    enabled: true
    handlers:
        - \Digilabscz\NetteLogger\Handlers\PushoverHandler('API_KEY', 'GROUP_KEY', 'RESTING_INTERVAL')
        - \Digilabscz\NetteLogger\Handlers\SlackHandler('HOOK_URL', 'RESTING_INTERVAL')
```

Example:
```neon
logger:
    enabled: true
    handlers:
        - \Digilabscz\NetteLogger\Handlers\PushoverHandler('a1111111111', 'g2222222222', '30 minutes')
        - \Digilabscz\NetteLogger\Handlers\SlackHandler('https://hooks.slack.com/services/xxxxxxxx/yyyyyyyy/zzzzzzzz', '30 minutes')
```

## Customization
You can make your own custom handler, just implement `\Digilabscz\NetteLogger\Handler` interface and register handler in logger configuration.


<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger\Handlers;

use DateTime;
use Digilabscz\NetteLogger\Handler;
use Digilabscz\NetteLogger\Message;
use Exception;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use Tracy\ILogger;

final class PushoverHandler implements Handler
{
    private const MAX_BACKTRACE_DEPTH = 6;
    
    private const LOGGABLE_LEVELS = [
        ILogger::WARNING,
        ILogger::EXCEPTION,
        ILogger::ERROR,
        ILogger::CRITICAL,
    ];

    /**
     * @param string $apiKey
     * @param string $groupKey
     * @param string $restingInterval
     */
    public function __construct(
        private readonly string $apiKey,
        private readonly string $groupKey,
        private readonly string $restingInterval,
    ) {}

    /**
     * @param DateTime $now
     * @param DateTime|null $lastExecution
     * @return bool
     * @throws Exception
     */
    public function isReady(DateTime $now, ?DateTime $lastExecution): bool
    {
        if (! $lastExecution) {
            return true;
        }

        $limit = (clone $lastExecution)->modify('+' . $this->restingInterval);

        return $now >= $limit;
    }

    /**
     * @param Message $message
     * @return void
     * @throws JsonException
     */
    public function handle(Message $message): void
    {
        if (! in_array($message->getLevel(), self::LOGGABLE_LEVELS, true)) {
            return;
        }

        $throwable = $message->getThrowable();
        $content = '<b>Message:</b><br>';
        $content .= Strings::truncate($throwable->getMessage(), 256);
        $content .= '<br>';
        $content .= '<br>';

        $content .= '<b>Stored log:</b><br>';
        $content .= '<a href="' . $message->getLogUrl() . '">' . $message->getId() . '</a>';
        $content .= '<br>';
        $content .= '<br>';

        $content .= '<b>Created on:</b><br>';
        $content .= $message->getCreatedOn()->format('j.n.Y, H:i:s');
        $content .= '<br>';
        $content .= '<br>';

        $content .= '<b>Backtrace:</b><br>';
        foreach (array_filter(explode(PHP_EOL, $throwable->getTraceAsString())) as $i => $line) {
            if ($i > self::MAX_BACKTRACE_DEPTH) {
                $content .= '<small><font color="#6a6a6a">...</font></small><br><br>';
                break;
            }
            
            $content .= '<small><font color="#6a6a6a">' . $line .  '</font></small><br><br>';
        }

        // send
        $ch = curl_init('https://api.pushover.net/1/messages.json');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, Json::encode([
            'token' => $this->apiKey,
            'user' => $this->groupKey,
            'title' => strtoupper($message->getDomain()) . ' – ' . strtoupper($message->getLevel()) . '!',
            'message' => $content,
            'html' => 1,
            'url' => $message->getLogUrl(),
        ]));
        curl_exec($ch);
        curl_close($ch);
    }
}

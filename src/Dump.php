<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger;

use Exception;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use ReflectionClass;
use ReflectionException;
use Throwable;

class Dump extends Exception
{
    /**
     * @var mixed
     */
    private mixed $value;

    /**
     * @param mixed $value
     * @param string $level
     * @throws JsonException
     */
    public function __construct(mixed $value, string $level)
    {
        $this->value = $value;
        $message = ucfirst($level) . ': ';

        if ($value === null) {
            $message .= '[NULL] > NULL';
        } elseif (is_string($value)) {
            $message .= '[string, length: ' . strlen($value) . '] > ' . $value;
        } elseif (is_int($value)) {
            $message .= '[int] > ' . $value;
        } elseif (is_float($value)) {
            $message .= '[float] > ' . $value;
        } elseif (is_bool($value)) {
            $message .= '[boolean] > ' . ($value ? 'TRUE' : 'FALSE');
        } elseif (is_array($value)) {
            $message .= '[array] > ' . Json::encode($value);
        } elseif ($value instanceof Throwable) {
            $message .= '[exception] > ' . $value->getMessage();
        } elseif (is_object($value)) {
            $message .= '[object][' . get_class($value) . '][' . spl_object_id($value). ']';
        }

        parent::__construct($message);

        try {
            $reflectionClass = new ReflectionClass(parent::class);
            $trace = $reflectionClass->getProperty('trace')->getValue($this);
            array_shift($trace);
            $reflectionClass->getProperty('trace')->setValue($this, $trace);
        } catch (ReflectionException) {}
    }

    /**
     * @return mixed
     */
    public function getValue(): mixed
    {
        return $this->value;
    }
}

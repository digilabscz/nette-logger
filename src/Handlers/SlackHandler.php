<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger\Handlers;

use DateTime;
use Digilabscz\NetteLogger\Handler;
use Digilabscz\NetteLogger\Message;
use Exception;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use Tracy\ILogger;

final class SlackHandler implements Handler
{
    private const MAX_BACKTRACE_DEPTH = 6;
    
    private const LOGGABLE_LEVELS = [
        ILogger::WARNING,
        ILogger::EXCEPTION,
        ILogger::ERROR,
        ILogger::CRITICAL,
    ];

    /**
     * @param string $hookUrl
     * @param string $restingInterval
     */
    public function __construct(
        private readonly string $hookUrl,
        private readonly string $restingInterval,
    ) {}

    /**
     * @param DateTime $now
     * @param DateTime|null $lastExecution
     * @return bool
     * @throws Exception
     */
    public function isReady(DateTime $now, ?DateTime $lastExecution): bool
    {
        if (! $lastExecution) {
            return true;
        }

        $limit = (clone $lastExecution)->modify('+' . $this->restingInterval);

        return $now >= $limit;
    }

    /**
     * @param Message $message
     * @return void
     * @throws JsonException
     */
    public function handle(Message $message): void
    {
        if (! in_array($message->getLevel(), self::LOGGABLE_LEVELS, true)) {
            return;
        }

        $throwable = $message->getThrowable();
        $content = '💥 *' . strtoupper($message->getDomain()) . ' – ' . strtoupper($message->getLevel()) . '!*';
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= '📜 *Message*';
        $content .= PHP_EOL;
        $content .= Strings::truncate($throwable->getMessage(), 256);
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= '🖇 *Stored log:*';
        $content .= PHP_EOL;
        $content .= '<' . $message->getLogUrl() . '|' . $message->getId() . '>';
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= '⌛️ *Created on:*';
        $content .= PHP_EOL;
        $content .= $message->getCreatedOn()->format('j.n.Y, H:i:s');
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= '🚀 *Backtrace:*';
        $content .= PHP_EOL;
        foreach (array_filter(explode(PHP_EOL, $throwable->getTraceAsString())) as $i => $line) {
            if ($i > self::MAX_BACKTRACE_DEPTH) {
                $content .= '_..._';
                break;
            }
            
            $content .= '_' . $line .  '_';
            $content .= PHP_EOL;
            $content .= PHP_EOL;
        }

        // send
        $ch = curl_init($this->hookUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, Json::encode([
            'text' => $content,
        ]));
        curl_exec($ch);
        curl_close($ch);
    }
}

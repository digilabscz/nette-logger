<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger;

use DateTime;
use Exception;
use Nette\Http\Request;
use Throwable;
use Tracy\BlueScreen;
use Tracy\Debugger;
use Tracy\ILogger;

class Logger implements ILogger
{
    /**
     * @var array|Handler[]
     */
    private array $handlers = [];

    /**
     * @var string|null
     */
    private ?string $directory = null;

    /**
     * @param Request $request
     */
    public function __construct(private readonly Request $request) {}

    /**
     * @return void
     * @throws Exception
     */
    public function initialize(): void
    {
        if (PHP_SAPI === 'cli') {
            set_exception_handler(function (Throwable $throwable) {
                $this->log($throwable, ILogger::EXCEPTION);
            });
        } else {
            $this->processRequest();
        }
    }

    /**
     * @param Handler $handler
     * @return $this
     */
    public function addHandler(Handler $handler): self
    {
        $this->handlers[] = $handler;

        return $this;
    }

    /**
     * @param mixed $value
     * @param string $level
     * @return void
     * @throws Exception
     */
    public function log(mixed $value, $level = self::INFO): void
    {
        $directory = $this->getDirectory();

        $value = $value instanceof Throwable ? $value : new Dump($value, $level);
        $id = $this->getLogID($value, $level);
        $fileDump = $directory . DS . $id . '.html';

        $schema = $this->request->getUrl()->getScheme();
        $domain = $this->request->getUrl()->getHost();
        $url = $schema . '://' . $domain . '?___logger_id=' . $id . '&___logger_check=' . $this->generateHash($id);

        if (! file_exists($fileDump)) {
            (new BlueScreen())->renderToFile($value, $fileDump);
        }

        $now = new DateTime();
        $message = new Message($id, $value, $level, $domain, $url);

        foreach ($this->handlers as $handler) {
            $fileStamp = $directory . DS . $id . '--' . $this->getClassShortName($handler::class) . '.timestamp';
            $lastExecution = file_exists($fileStamp)
                ? (clone $now)->setTimestamp((int) file_get_contents($fileStamp))
                : null;

            if ($handler->isReady($now, $lastExecution)) {
                $handler->handle($message);
                 file_put_contents($fileStamp, $now->getTimestamp());
            }
        }
    }

    /**
     * @return void
     */
    private function processRequest(): void
    {
        if (empty($_GET['___logger_id'])) {
            return;
        }

        if (empty($_GET['___logger_check'])) {
            header('HTTP/1.0 403 Forbidden');
            die('403');
        }

        $id = (string) $_GET['___logger_id'];
        $hash = $this->generateHash($id);

        if ($hash !== $_GET['___logger_check']) {
            header('HTTP/1.1 404 Not Found');
            die('404');
        }

        $path = $this->getDirectory() . DS . $id . '.html';
        if (! file_exists($path)) {
            header('HTTP/1.1 410 Gone');
            die('410');
        }

        echo file_get_contents($path);
        die();
    }

    /**
     * @return string
     */
    private function getDirectory(): string
    {
        if ($this->directory === null) {
            return rtrim(Debugger::$logDirectory, DIRECTORY_SEPARATOR);
        }

        return $this->directory;
    }

    /**
     * @param string $id
     * @return string
     */
    private function generateHash(string $id): string
    {
        return md5(__DIR__ . '//' . $id);
    }

    /**
     * @param Throwable $value
     * @param string $level
     * @return string
     */
    private function getLogID(Throwable $value, string $level): string
    {
        $path = [];
        $exception = $value;

        while ($exception) {
            $path[] = [
                get_class($exception),
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getFile(),
                $exception->getLine(),
                array_map(static function (array $item): array {
                    unset($item['args']);

                    return $item;
                }, $exception->getTrace()),
            ];
            $exception = $exception->getPrevious();
        }

        return md5(serialize($path)) . '--' . $level;
    }

    /**
     * @param string $class
     * @return string
     */
    private function getClassShortName(string $class): string
    {
        $parts = explode('\\', $class);

        return end($parts);
    }
}

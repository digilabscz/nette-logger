<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger;

use DateTime;

interface Handler
{
    /**
     * @param DateTime $now
     * @param DateTime|null $lastExecution
     * @return bool
     */
    public function isReady(DateTime $now, ?DateTime $lastExecution): bool;

    /**
     * @param Message $message
     * @return void
     */
    public function handle(Message $message): void;
}

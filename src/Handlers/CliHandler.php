<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger\Handlers;

use DateTime;
use Digilabscz\NetteLogger\Handler;
use Digilabscz\NetteLogger\Message;
use Nette\Utils\Strings;

final class CliHandler implements Handler
{
    private const COLOR_RED = "\033[0;31m";
    private const COLOR_ORANGE = "\033[0;33m";
    private const COLOR_NONE = "\033[0m";

    /**
     * @param DateTime $now
     * @param DateTime|null $lastExecution
     * @return bool
     */
    public function isReady(DateTime $now, ?DateTime $lastExecution): bool
    {
        return true;
    }

    /**
     * @param Message $message
     * @return void
     */
    public function handle(Message $message): void
    {
        if (PHP_SAPI !== 'cli') {
            return;
        }

        $throwable = $message->getThrowable();
        $content = self::COLOR_RED . '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' . self::COLOR_NONE;
        $content .= PHP_EOL;
        $content .= self::COLOR_RED . strtoupper($message->getDomain()) . ' – ' . strtoupper($message->getLevel()) . '!' . self::COLOR_NONE;
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= self::COLOR_ORANGE . 'Message:' . self::COLOR_NONE;
        $content .= PHP_EOL;
        $content .= Strings::truncate($throwable->getMessage(), 256);
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= self::COLOR_ORANGE . 'Stored log:' . self::COLOR_NONE;
        $content .= PHP_EOL;
        $content .= $message->getLogUrl();
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= self::COLOR_ORANGE . 'Created on:' . self::COLOR_NONE;
        $content .= PHP_EOL;
        $content .= $message->getCreatedOn()->format('j.n.Y, H:i:s');
        $content .= PHP_EOL;
        $content .= PHP_EOL;

        $content .= self::COLOR_ORANGE . 'Backtrace:' . self::COLOR_NONE;
        $content .= PHP_EOL;
        foreach (array_filter(explode(PHP_EOL, $throwable->getTraceAsString())) as $line) {
            $content .= $line;
            $content .= PHP_EOL;
        }

        $content .= self::COLOR_RED . '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~' . self::COLOR_NONE;
        $content .= PHP_EOL;

        echo $content;
    }
}

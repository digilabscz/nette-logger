<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger\DI;

use Digilabscz\NetteLogger\Handlers\CliHandler;
use Digilabscz\NetteLogger\Logger;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\PhpGenerator\ClassType;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Tracy\Debugger;

class LoggerExtension extends CompilerExtension
{
    /**
     * @return Schema
     */
    public function getConfigSchema(): Schema
	{
		return Expect::structure([
            'enabled' => Expect::bool(false),
            'handlers' => Expect::arrayOf(Statement::class),
		]);
	}

    /**
     * @return void
     */
    public function loadConfiguration(): void
	{
        $config = $this->config;

        if (! $config->enabled) {
            return;
        }

        $builder = $this->getContainerBuilder();
        if ($builder->hasDefinition('tracy.logger')) {
			$builder
                ->getDefinition('tracy.logger')
				->setAutowired(false);
		}

        $definition = $builder
            ->addDefinition('app.logger')
            ->setFactory(Logger::class, [])
            ->setType(Logger::class);

        $definition->addSetup(new Statement('$service->addHandler(?)', [
            new Statement(CliHandler::class, [])
        ]));

        foreach ($config->handlers as $handler) {
            $definition->addSetup(new Statement('$service->addHandler(?)', [$handler]));
        }
	}

    /**
     * @param ClassType $class
     * @return void
     */
    public function afterCompile(ClassType $class): void
	{
        $config = $this->config;
		$builder = $this->getContainerBuilder();
		$initialize = $class->getMethod('initialize');

		if ($builder->hasDefinition('tracy.logger')) {
			$initialize->addBody('$this->getService("tracy.logger");');
		}

		if ($config->enabled) {
			$initialize->addBody('$digilabsczLogger = $this->getService(?);', ['app.logger']);
			$initialize->addBody(Debugger::class . '::setLogger($digilabsczLogger);');
			$initialize->addBody('$digilabsczLogger->initialize();');
		}
	}
}

<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger;

use DateTime;
use Throwable;

class Message
{
    /**
     * @var DateTime
     */
    private readonly DateTime $createdOn;

    /**
     * @param string $id
     * @param Throwable $throwable
     * @param string $level
     * @param string $domain
     * @param string $logUrl
     */
    public function __construct(
        private readonly string $id,
        private readonly Throwable $throwable,
        private readonly string $level,
        private readonly string $domain,
        private readonly string $logUrl,
    ) {
        $this->createdOn = new DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Throwable
     */
    public function getThrowable(): Throwable
    {
        return $this->throwable;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @return string
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getLogUrl(): string
    {
        return $this->logUrl;
    }

    /**
     * @return DateTime
     */
    public function getCreatedOn(): DateTime
    {
        return $this->createdOn;
    }
}

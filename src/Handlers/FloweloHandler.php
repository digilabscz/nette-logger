<?php declare(strict_types=1);

namespace Digilabscz\NetteLogger\Handlers;

use DateTime;
use Digilabscz\NetteLogger\Handler;
use Digilabscz\NetteLogger\Message;
use Exception;
use Nette\Utils\Strings;
use Tracy\ILogger;

final class FloweloHandler implements Handler
{
    private const LOGGABLE_LEVELS = [
        ILogger::WARNING,
        ILogger::EXCEPTION,
        ILogger::ERROR,
        ILogger::CRITICAL,
    ];

    /**
     * @param string $restingInterval
     * @param string $apiKey
     * @param int|string $project
     * @param int|string $author
     * @param int|string $solver
     * @param string $billingMode
     * @param int $priority
     * @param int $hoursEstimated
     */
    public function __construct(
        private readonly string $restingInterval,
        private readonly string $apiKey,
        private readonly int|string $project,
        private readonly int|string $author,
        private readonly int|string $solver,
        private readonly string $billingMode = 'non-billable',
        private readonly int $priority = 2,
        private readonly int $hoursEstimated = 1,
    ) {}

    /**
     * @param DateTime $now
     * @param DateTime|null $lastExecution
     * @return bool
     * @throws Exception
     */
    public function isReady(DateTime $now, ?DateTime $lastExecution): bool
    {
        if (! $lastExecution) {
            return true;
        }

        $limit = (clone $lastExecution)->modify('+' . $this->restingInterval);

        return $now >= $limit;
    }

    /**
     * @param Message $message
     * @return void
     */
    public function handle(Message $message): void
    {
        if (! in_array($message->getLevel(), self::LOGGABLE_LEVELS, true)) {
            return;
        }

        $lever = Strings::firstUpper($message->getLevel());
        $domain = Strings::firstUpper($message->getDomain());

        $throwable = $message->getThrowable();
        $content = '<h3>💥 ' . $domain . '</h3>';
        $content .= '<p>' . $lever . '</p>';

        $content .= '<br>';
        $content .= '<h3>📜 Message</h3>';
        $content .= '<p>' . $throwable->getMessage() . '</p>';

        $content .= '<br>';
        $content .= '<h3>🖇 Stored log:</h3>';
        $content .= '<p><a href="' . $message->getLogUrl() . '" target="_blank">' . $message->getId() . '</a></p>';

        $content .= '<br>';
        $content .= '<h3>⌛️ Created on:</h3>';
        $content .= '<p>' . $message->getCreatedOn()->format('j.n.Y, H:i:s') . '</p>';

        $content .= '<br>';
        $content .= '<h3>🚀 Backtrace:</h3>';
        foreach (array_filter(explode(PHP_EOL, $throwable->getTraceAsString())) as $line) {
            $content .= '<p style="color: gray; line-height: 1.5rem;"><i><small>' . $line . '</small></i></p>';
        }

        $parameters = [
            'title' => $domain . ', ' . $message->getLevel() . ': ' . $throwable->getMessage(),
            'description' => $content,
            'project' => $this->project,
            'author' => $this->author,
            'solver' => $this->solver,
            'dateBegin' => 'today',
            'dateDue' => '+2 days',
            'billingMode' => $this->billingMode,
            'priority' => $this->priority,
            'hoursEstimated' => $this->hoursEstimated,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://www.flowelo.com/api/workspace/task-create?apiKey=' . $this->apiKey);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($parameters));
        curl_exec($curl);
        curl_close($curl);
    }
}
